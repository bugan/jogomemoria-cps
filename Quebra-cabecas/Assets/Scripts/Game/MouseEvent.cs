﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseEvent : MonoBehaviour {
	private static MouseEvent s_Instance = null;

	public static MouseEvent Instance{
		get{
			if(s_Instance == null)
				s_Instance = FindObjectOfType(typeof(MouseEvent)) as MouseEvent;

			if(s_Instance == null){
				GameObject obj = new GameObject("MouseEvent");
				s_Instance = obj.AddComponent(typeof(MouseEvent)) as MouseEvent;
				Debug.Log("Could not locate an [MouseEvent] object. \n [MouseEvent] was generated Automaticly.");
			}
			return s_Instance;
		}
	}


	[HideInInspector]public Transform collidingTransform;
	[HideInInspector]public uint currentColliding;
	Transform pecaAtual;
	bool mouseDown;
	void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			mouseDown = true;
			RaycastHit2D hit = Physics2D.Raycast((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward,50);	
			if(hit.collider != null && hit.collider.CompareTag("peca"))
			{
				if(!hit.collider.GetComponent<Peca>().locked)
				{
					pecaAtual = hit.collider.GetComponent<Transform>();
					hit.collider.GetComponent<SpriteRenderer>().sortingOrder = 1;
				}
			}
			else
				pecaAtual = null;
		}
		if(Input.GetButtonUp("Fire1"))
		{
			mouseDown = false;
			if(pecaAtual != null)
			{
				pecaAtual.GetComponent<SpriteRenderer>().sortingOrder = 0;
				Peca p = pecaAtual.GetComponent<Peca>();
				if(this.currentColliding == p.numeroPeca)
				{
					pecaAtual.position = collidingTransform.position;
					p.locked = true;
				}
				this.currentColliding = 0;
				pecaAtual = null;
			}
		}

		if(pecaAtual != null)
		{
			pecaAtual.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}			
	}

	void OnApplicationQuit(){
		s_Instance = null;
	}

}
