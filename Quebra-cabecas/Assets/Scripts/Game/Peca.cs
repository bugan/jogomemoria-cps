﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peca : MonoBehaviour {

	[Range(1,99)]public uint numeroPeca = 1;
	public bool locked;

	void Start()
	{

		Debug.Log("teste");
		GameObject obj = GameObject.Find("Area Pecas");
		BoxCollider2D collider = obj.GetComponent<BoxCollider2D>();
		Transform t = obj.GetComponent<Transform>();

		this.transform.position = new Vector3((t.position.x -collider.size.x/2)+Random.Range(0,collider.size.x),(t.position.y-collider.size.y/2) +Random.Range(0,collider.size.y),0);

	}
}
