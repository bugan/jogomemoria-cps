﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPosition : MonoBehaviour {

	[Range(1,99)]public uint numeroPeca;

	void OnTriggerEnter2D(Collider2D other)
	{
		MouseEvent.Instance.currentColliding = this.numeroPeca;
		MouseEvent.Instance.collidingTransform = this.transform;
	}

}
