﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
	private static Manager s_Instance = null;

	public static Manager Instance{
		get{
			if(s_Instance == null)
				s_Instance = FindObjectOfType(typeof(Manager)) as Manager;

			if(s_Instance == null){
				GameObject obj = new GameObject("Manager");
				s_Instance = obj.AddComponent(typeof(Manager)) as Manager;
				Debug.Log("Could not locate an [Manager] object. \n [Manager] was generated Automaticly.");
			}
			return s_Instance;
		}
	}

	[TextArea (10 ,15)] public string TextoCreditos;
	[TextArea(10 ,15) ] public string TextoSobre;

	void OnApplicationQuit(){
		s_Instance = null;
	}

}
