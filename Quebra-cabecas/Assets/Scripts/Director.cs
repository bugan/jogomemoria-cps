﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Director : MonoBehaviour {

	public Text conteiner;
	public void loadScene(string name)
	{
		SceneManager.LoadScene(name);
	}

	public void clickButton(string name)
	{
		if(name == "creditos")
			this.conteiner.text = Manager.Instance.TextoCreditos;
		else
			this.conteiner.text = Manager.Instance.TextoSobre;
	}
}
